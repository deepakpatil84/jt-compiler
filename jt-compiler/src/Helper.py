"""
/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
 """
from HTMLParser import WAFHTMLParser
from HTMLParser import HTMLViewNode
from HTMLParser import HTMLView
import sys,os
def getCssClasses(rnode, cls):    
    
    if rnode.nodeType in [HTMLViewNode.NODE_COMMENT,HTMLViewNode.NODE_DECL]:return
    for atr in rnode.attributes:        
        if atr[0] == "class" or atr[0] == "CLASS":
            v=atr[1]            
            if not cls.has_key(v):
                cls[v]=1
    for cnode in rnode.childNodes:                                
        getCssClasses(cnode,cls)
def getIds(rnode, cls):    
    if rnode.nodeType in [HTMLViewNode.NODE_COMMENT,HTMLViewNode.NODE_DECL]:return
    for atr in rnode.attributes:        
        if atr[0] == "id" or atr[0] == "ID":
            v=atr[1]            
            if not cls.has_key(v):
                name=rnode.name
                if name.startswith("WAF:"):
                    name=name[4:]
                else:
                    name=name.capitalize()+"Element"                    
                cls[v]=name
            else:
                raise Exception("Duplicate ID Found  "+v) 
    for cnode in rnode.childNodes:                                
        getIds(cnode,cls)
if __name__=="__main__":
    
    if len(sys.argv)==3:
       
        o=sys.argv[1]        
        if o=="css":
            hv = WAFHTMLParser(sys.argv[2],False)
            cls={}
            getCssClasses(hv.rootNode, cls)
            for k in cls:
                print k
        if o=="cssf":
            hv = WAFHTMLParser(sys.argv[2],False)
            cls={}
            getCssClasses(hv.rootNode, cls)
            for k in cls:
                print "."+k[1:-1]+"{"
                print "}"
        if o=="id":
            hv = WAFHTMLParser(sys.argv[2],False)
            cls={}
            getIds(hv.rootNode, cls)
            for k in cls:
                print k
        if o=="idf":
            hv = WAFHTMLParser(sys.argv[2],False)
            cls={}
            getIds(hv.rootNode, cls)            
            s = "";
            for k in cls:
                s+="\t@ViewElement\n"
                s+="\tprivate "+cls[k]+" "+k[1:-1]+";\n"
                s+="\n"
            #os.system("echo \""+s+"\"| clip")
            print s
            
