"""
/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
 """
from HTMLParser import WAFHTMLParser
from HTMLParser import HTMLViewNode
from HTMLParser import HTMLView 
from ConfigReader import WAFConfig, LibConfig
from Analyzer import SHelper, SGlobal, SClass, ExField
from JavaLangAST import ExClass

def markRefsForControl(view_file, cls):
    hv = WAFHTMLParser(view_file, True)
    cons = cls.getConstructor([], None)
    if cons == None:
        raise Exception("No default constructor found for " + cls.fullname + " in " + view_file) 
    markRefsForView(hv.rootNode, cls, cons)

def processControl(view_file, cls):
    hv = WAFHTMLParser(view_file, True)
    ids = {}
    id_list = [0]
    id_list.append({})
    getIds(hv.rootNode, ids, id_list)
    #for id in ids:
    #field = cls.getFieldWithThisAccess(id, False, cls)
    ids_to_look = ids.keys()
    
    for m in cls.exmembers:
        if isinstance(m, ExField):
            if m.field.getAnnotation("ViewElement"):
                if not ids.has_key(m.field.name):                    
                    exdata = "\n View File:"+view_file + "\n Class:" + cls.fullname +"\n "                    
                    raise Exception("Field '"+m.field.name +"'declared as view element but didn't find corresponding view element."+exdata)
                else:
                    ids_to_look.remove(m.field.name)
    
    if len(ids_to_look) > 0:
        exdata = "\n View File:"+view_file + "\n Class:" + cls.fullname +"\n "
        print "Warning:Ids not declared as ViewElement " + ",".join(ids_to_look) + exdata         
    #print "XX",ids
    code = []
    correctViewForCommonErrors(hv.rootNode)
    getHTMLViewCodeWithoutID(hv.rootNode, code)
    fcode = []
    line = "".join(code)
    line = line.replace("\"", "\\\"")
    fcode.append("var c=null,m=null,t=this,e,g=$w._cb.g,d;")
    #due to some error in IE we need separate div for each element creation  in other browser single global div is sufficient
    fcode.append("d=document.createElement('div');");
    fcode.append("d.innerHTML=\"" + line + "\";");        
    fcode.append("e=t.elm=d.firstChild;");
    fcode.append("e._wo_=t;");       
    code = getJavaScriptForView(hv.rootNode, [], cls, id_list)
    fcode = fcode + code
    return fcode

def containsId(a):
    rvalue = False
    for i in a:
        if i[0] == "id" or i[0] == "ID":
            rvalue = True
            break
    return rvalue

def containsIdGet(a):
    for i in a:
        if i[0] == "id" or i[0] == "ID":  
            n = i[1]
            if n == None:
                raise Exception("id is none")
            return removeQuote(n)    
    raise Exception("id not found " + a)

def removeQuote(a):
    rvalue = a 
    if a.startswith("\""):
        rvalue = a[1:-1]
    return rvalue        

def getIds(rootNode, ids, id_count):    
    if not( rootNode.nodeType in [HTMLViewNode.NODE_COMMENT, HTMLViewNode.NODE_DECL] ):        
        for ch in rootNode.childNodes:
            getIds(ch, ids, id_count) 
        if containsId(rootNode.attributes):        
            value = containsIdGet(rootNode.attributes)
            if ids.has_key(value):
                raise Exception("Exception duplicate id " + value)
            ids[value] = rootNode
            id_count[1][rootNode] = "this." + value
            rootNode.id = value
        elif rootNode.name.startswith("WAF:"):
            i = "__wid_" + str(id_count[0])
            id_count[0] += 1
            rootNode.attributes.append(["id", i])
            id_count[1][rootNode] = i
            ids[i] = rootNode
            rootNode.id = i
    
def correctViewForCommonErrors(rootNode):
    """
    1 table must contain table body if not present it will be added automatically
    """
    if not ( rootNode.nodeType in [HTMLViewNode.NODE_COMMENT, HTMLViewNode.NODE_DECL] ):
        if rootNode.name.lower() == "table":
            last_section = None
            l = len(rootNode.childNodes)
            nnodes = []
            for i in range(l):
                ch = rootNode.childNodes[i]
                if ch.name.lower() == "tr":
                    if last_section == None:
                        last_section = HTMLViewNode()
                        last_section.name = "tbody"
                        nnodes.append(last_section)
                    last_section.childNodes.append(ch)
                    correctViewForCommonErrors(ch)
                else:
                    last_section = None
                    nnodes.append(ch)
                    correctViewForCommonErrors(ch)           
            rootNode.childNodes = nnodes
        else: 
            for ch in rootNode.childNodes:
                correctViewForCommonErrors(ch)
                    
        
def getHTMLViewCodeWithoutID(rnode, code):
    if rnode.nodeType in [HTMLViewNode.NODE_COMMENT, HTMLViewNode.NODE_DECL]:return
    if rnode.nodeType == HTMLViewNode.NODE_DATA:
        if len(rnode.innerHTML.strip()) != 0:
            code.append(rnode.innerHTML.strip() + " ")                
    else:
        if len(rnode.name.strip()) != 0:
            if rnode.name.lower() == "style":
                """
                if len(rnode.childNodes)>0:
                    cdata = []
                    for n in rnode.childNodes:
                        cdata.append(n.innerHTML)
                    cdata = "".join(cdata)                     
                    cp = SGlobal.css_cp
                    cp.init(cdata)
                    [selectors,macros]=cp.readClass()
                    gmacros = SGlobal.css_macros
                    
                    if gmacros:
                        for m in gmacros:
                            macros[m]=gmacros[m]
                                            
                    data =SGlobal.css_data
                    for sel in selectors:
                        data.append('\n')
                        data.append( cp.processSelector(sel, macros, {}) )                        
                    #print "".join(data)
                """
                return
                
            if rnode.name.startswith("WAF:"):
                s = "<div"
            else:
                s = "<" + rnode.name
            if not rnode.name.startswith("WAF:"):
                for atr in rnode.attributes:
                    name = atr[0]
                    value = atr[1]
                    if not ( name == "id" or name == "ID"):
                        s += " " + name
                        if value != None:
                            s += "=\"" + removeQuote(value) + "\""
    
            if len(rnode.childNodes) > 0:
                s += ">"
                code.append(s)
            else:
                if rnode.name.startswith("WAF:"):
                    s += "></div>"
                elif rnode.name.lower() in ["div" , "script", "span", "ol", "ul", "textarea", "iframe", "table","title" ]:
                    s += "></" + rnode.name + ">" #need to investigate might be some bug in browser in rendering this
                else:
                    s += "/>"
                code.append(s)
                return
    
        if (len(rnode.name.strip()) != 0):
            if rnode.name.startswith("WAF:"):
                for cnode in rnode.childNodes:
                    if cnode.nodeType == HTMLViewNode.NODE_ELEMENT:
                        code.append("<div>")
                        for cc in cnode.childNodes:
                            getHTMLViewCodeWithoutID(cc, code)
                            code.append("</div>")
                code.append("</div>")
                return 
        for cnode in rnode.childNodes:
            getHTMLViewCodeWithoutID(cnode, code)
        if len(rnode.name.strip()) != 0:
            code.append("</" + rnode.name + ">")
                
def addMethodRef(method, mid):
    #TODO:remove
    if mid == 4218:
        print method.name
        sdfsdf
    if mid not in method.refs:
        method.refs.append(mid)
    if not SGlobal.method_refs.has_key(method.mid):
        SGlobal.method_refs[method.mid] = method
def markRefsForView(node, cls, cur_method):
    count = 0
    for cnode in node.childNodes:
        markRefsForView(cnode, cls, cur_method)
        count += 1
    if node.name.startswith("WAF:"):
        cname = node.name[4:]
        control_cls = SHelper.getClassOnNameFromImported(cname, cls)
        if control_cls == None:
            raise Exception("Class not found with name ", cname)
        cons = control_cls.getConstructor([], None)
        if cons == None:
            raise Exception("Class dont have default constructor " + control_cls.fullname)
        addMethodRef(cur_method, cons.mid)
        for atr in node.attributes:
            name = atr[0]
            if name.lower() != "id":
                #name = "set" + name[0].upper() + name[1:]
                #m = control_cls.getMethodWithThisAccess(name, [SGlobal.stringclass.mytype], False, None, cls)
                m = control_cls.getAttributeMethod(name)
                if m != None:
                    addMethodRef(cur_method, m.method.mid)
        for cn in node.childNodes:
            if cn.nodeType == HTMLViewNode.NODE_ELEMENT:            
                name = cn.name                
                types = getMethodArgsTypes(cn.childNodes, cls)                
                m = control_cls.getMethodWithThisAccess(name, types, False, None, cls)
                if m == None:
                    m = control_cls.getMethodWithThisAccess(name, [SGlobal.stringclass.mytype], False, None, cls)
                    if m==None:
                        raise Exception(cls.fullname + "(View Error)Method not found " + name + " for class " + control_cls.fullname +'Types:'+str(types))
                addMethodRef(cur_method, m.method.mid)
def getJavaScriptForView(node, arr, cls, id_list):
    count = 0
    code = []
    for cnode in node.childNodes:
        arr.append(int(count))
        code += getJavaScriptForView(cnode, arr, cls, id_list)
        arr.pop()
        count += 1
    if node.id != None:
        if node.name.startswith("WAF:"):
            value = containsIdGet(node.attributes)
            cname = node.name[4:]
            control_cls = SHelper.getClassOnNameFromImported(cname, cls)
            if control_cls == None:
                raise Exception("Class not found with name ", cname)
            cons = control_cls.getConstructor([], None)
            if cons == None:
                raise Exception("Class don't have default constructor. UIControl must have default constructor in order to create through markup" + control_cls.fullname)
            #line = "(new " + control_cls.getPrototype() + "())." + cons.getJSName() + "()"
            line = "new " + control_cls.getPrototype() + "()"
            varname = value;
            if value.startswith("__wid_"):
                code.append("var " + value + "=" + line + ";")
            else:
                field = cls.getFieldWithThisAccess(value, False, cls)
                if field == None:
                    raise Exception("Field '"+value +"' not declared in class " + cls.fullname )
                field = field.field            
                code.append("t." + field.getJSName() + "=" + line + ";")
                varname = "t." + field.getJSName()
            s = "c=g(e,["
            s += ",".join([str(n) for n in arr[1:]])
            s += "]);"
            code.append(s)
            code.append(WAFConfig.getWAFRootObjectName() + "._cb.rn(c," + varname + ".elm);")
            code.append(varname + "." + cons.getJSName() + "();")
            
            for a in node.attributes:
                atr_name = a[0]            
                
                if atr_name.lower() != "id":
                    atr_value = removeQuote(a[1])
                    #name = "set" + atr[0:1].upper() + atr[1:]
                    
                    #m = control_cls.getMethodWithThisAccess(name, [SGlobal.stringclass.mytype], False, None, cls)
                    m = control_cls.getAttributeMethod(atr_name)
                    if m!=None:
                        print "Attr-Name:",atr_name,m.method.name
                    else:
                        print "No method found",atr_name
    
                    if m == None:
                        code.append(WAFConfig.getWAFRootObjectName() + "._cb.sa(" + varname + ".elm,\"" + atr_name + "\",\"" + atr_value + "\");")
                    else:
                        code.append(varname + "." + m.method.getJSName() + "(\"" + atr_value + "\");")
                  
              
            mcode = []
            mcode.append("m=" + WAFConfig.getWAFRootObjectName() + "._cb.ge(c);")
            mcount = 0
            for cn in node.childNodes:
                if cn.nodeType == HTMLViewNode.NODE_ELEMENT:            
                    name = cn.name
                    childs = getNonEmptyChilds(cn)
                    count = len(childs)
                    #TODO:optimize this
                    t = SHelper.getClassOnFullName(LibConfig.P_WEB_DOM_ELM).mytype
                    types = getMethodArgsTypes(cn.childNodes, cls)
                    #for i in range(count):
                    #    types.append(t)
                    m = control_cls.getMethodWithThisAccess(name, types, False, None, cls)
                    call_as_string=False
                    if m == None:
                        call_as_string = True
                        types = [SGlobal.stringclass.mytype]
                        m = control_cls.getMethodWithThisAccess(name,types, False, None, cls)
                        if m == None:
                            raise Exception(cls.fullname + "(View Error)Method not found " + name + " for class " + control_cls.fullname)
                    if call_as_string == True:
                        line = varname + "." + m.method.getJSName() + "(m[" + str(mcount) + "].innerText || m[" + str(mcount) + "].textContent );"
                    else:     
                        mcode.append("var a=" + WAFConfig.getWAFRootObjectName() + "._cb.ge(m[" + str(mcount) + "]);")
                        line = varname + "." + m.method.getJSName() + "("
                        pars = []
                        for i in range(count):
                            if types[i] == t:
                                pars.append("a[" + str(i) + "]")
                            else:
                                if id_list[1].has_key(childs[i]):
                                    if containsId(childs[i].attributes):
                                        fname = containsIdGet(childs[i].attributes)                            
                                        if not fname.startswith("__wid"):                                                    
                                            field = cls.getField(fname)
                                            pars.append("t." + field.getJSName())
                                        else:
                                            pars.append(fname)
                                    else:                    
                                        pars.append(id_list[1][childs[i]])
                                else:
                                    raise Exception("UnExpected Something went wrong please report this.")
                        line = line + ",".join(pars) + ");"
                    mcode.append(line)
                    mcount += 1
            if len(mcode) > 1:
                code.append(mcode)
            
        else:        
            field = cls.getFieldWithThisAccess(node.id, False, cls)
            if field == None:                
                raise Exception("Field '"+node.id +"' not declared in class " + cls.fullname )
            field = field.field     
            s = "t." + field.getJSName() + "=g(e,["
            s += ",".join([str(n) for n in arr[1:]])
            s += "]);"
            code.append(s)      
    return code
def countNonEmptyChilds(rnode):
    count = 0
    for ch in rnode.childNodes:
        if ch.nodeType == HTMLViewNode.NODE_ELEMENT:
            count += 1
        elif ch.nodeType == HTMLViewNode.NODE_DATA:
            if ch.innerHTML != None and len(ch.innerHTML.strip()) > 0:
                count += 1
    return count
def getNonEmptyChilds(rnode):
    cs = []
    for ch in rnode.childNodes:
        if ch.nodeType == HTMLViewNode.NODE_ELEMENT:
            cs.append(ch)
        elif ch.nodeType == HTMLViewNode.NODE_DATA:
            if ch.innerHTML != None and len(ch.innerHTML.strip()) > 0:
                cs.append(ch)
    return cs
def getMethodArgsTypes(nodes, cls):
    rvalue = None
    if nodes == None:
        rvalue = []
    else:
        types = []
        t = SHelper.getClassOnFullName(LibConfig.P_WEB_DOM_ELM).mytype
        for cn in nodes:
            if cn.nodeType == HTMLViewNode.NODE_ELEMENT:
                if cn.name.startswith("WAF:"):
                    name = cn.name[4:]
                    a = SHelper.getClassOnNameFromImported(name, cls)
                    if a == None:
                        raise Exception("Class not found in view " + name)
                    types.append(a.mytype)            
                else:
                    types.append(t)
        rvalue = types
    return rvalue

