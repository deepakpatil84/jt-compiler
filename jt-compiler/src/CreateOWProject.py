import sys
import os

def create_net_beans_project(root):
	os.makedirs(root+"/src/java/client")
	os.makedirs(root+"/src/java/controllers")
	os.makedirs(root+"/src/java/server/services")
	os.makedirs(root+"/src/java/shared")
	fh=open(root+"/WebContent/JsTypeConfig.xml","w")
	fh.write("""<?xml version="1.0" encoding="UTF-8"?>
<jstype-web-app>
	<target-path>E:/Personal/JsTypePublic/projects/SampleApplications/Chat_Compiled/</target-path>
    <lib-paths>
        <path>E:/Personal/JsTypePublic/projects/_WAFJavaLib/src/</path>
        <path>E:/Personal/JsTypePublic/projects/_WAFLib/src/</path>
    </lib-paths>
	<target-ide>netbeans</target-ide><!-- netbeans -->
	<target-web-app-server>apache</target-web-app-server><!-- glassfish,jboss,websphere -->
	<compile-profile-name>test</compile-profile-name><!-- deploy -->
	<proxy-servlet-url>WAFProxy</proxy-servlet-url>
    <compiled-js-file>open-waf-compiled.js</compiled-js-file>
    <style-sheets>
        <style-sheet>jstype-modern.css</style-sheet>
    </style-sheets>
	<profile name="test">
		<compatible-with-ie6>true</compatible-with-ie6>
		<waf-root-object-name>$w</waf-root-object-name>
		<add-prefix-to-gen-code>false</add-prefix-to-gen-code>
		<gen-line-code-prefix> </gen-line-code-prefix>
		<remove-unused-code>true</remove-unused-code>
		<mark-unused-code>true</mark-unused-code>
		<unused-code-marker>/** UNUSED **/</unused-code-marker>
		<enable-array-getter-setter>true</enable-array-getter-setter><!-- this code will work much faster but will not throw ArrayIndexOutOfBoundsException -->
		<getter-setter-optimization>false</getter-setter-optimization>
	</profile>
	<profile name="deploy">
		<compatible-with-ie6>true</compatible-with-ie6>
		<waf-root-object-name>$w</waf-root-object-name>
		<obfuscate>true</obfuscate>
		<minify>true</minify>
		<remove-unused-code>true</remove-unused-code>
		<enable-array-getter-setter>true</enable-array-getter-setter>
		<getter-setter-optimization>true</getter-setter-optimization>
		<aggregate-strings>true</aggregate-strings>
	</profile>
</jstype-web-app>
			 """)
	fh.close()	

if __name__=="__main__":
	[sdir,sep,rest]=sys.argv[0].rpartition(os.path.sep)
	print "S Dir",sdir
	[pdir,sep,rest]=sdir.rpartition(os.path.sep)
	print "P Dir",pdir
	create_net_beans_project(sys.argv[1])