'''
Created on Sep 29, 2013

@author: Deepak Patil
'''
import sys
import os
import Compiler
import Analyzer


from ConfigReader import WAFConfig, LibConfig
        
class AppConfig:
    libs=[]

    @staticmethod
    def readConfig(path):
        fh=open(path,"r")
        lines = fh.readlines()
        for line in lines:
            AppConfig.libs.append(line.strip())
        AppConfig.libs.append(".")
            
    @staticmethod
    def getLibs():
        return AppConfig.libs
            
def compileApp():
    sys.setrecursionlimit(40000)
    AppConfig.readConfig("AppConfig.xml")
    LibConfig.MODE="LIB"
    cus=Compiler.compileLibs(AppConfig.getLibs())
    
    content=Compiler.getJSForLibs(cus)
    if len(sys.argv)>1:
        fh=open(sys.argv[1],"w")
        fh.write(content)
        fh.close()
    else:
        print content 
    
if __name__=="__main__":
    compileApp()