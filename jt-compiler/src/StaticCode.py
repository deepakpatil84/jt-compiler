"""
/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
 """
from ConfigReader import WAFConfig

def getCode():
    
    #TODO:these function needs to be added in framework classes
    root=WAFConfig.getWAFRootObjectName()
    code=[]
    code.append("var $cc="+root+"._cc={};\n")
    if WAFConfig.isJSDebuggingEnabled():
        code.append(root+"""._d=function(fid,mid,lineno){
              window.__fid=fid;
              window.__mid=mid;
              window.__lineno=lineno;
              debugger;
            };""")
        code.append(root+"""._mi=function(mid){
              window.__mi=mid;
              debugger;
            };""")
        code.append(root+"""._mo=function(mid){
              window.__mo=mid;
              debugger;
            };""")
        code.append(root+"""._ps=function(mid){              
              window.__preserve_stack=1;              
            };""")
        
    code.append(root+""".createArray=function(sizes,iv,ci){
        var l,a,i;
		if(ci==undefined) ci=0;
		if(iv==undefined) iv=null;
		if(sizes.length==0){
			return [];	
		}
	    l=sizes[ci];a=[];i=0;
        if((sizes.length-1)>ci){
	        for(i=0;i<l;i++){
    	        a.push(this.createArray(sizes,iv,ci+1));
        	}
        }else{
	        for(i=0;i<l;i++){
    	        a.push(iv);
        	}
		}
        return a;
    };""")
    code.append(root+"._cb={};\n")
    code.append(root+"""._cb.ge=function(r){
        var i,cn,l,es=[]; 
        try{
            l=r.childNodes.length;
            for(i=0;i<l;i++){
                cn=r.childNodes[i];
                if(cn.nodeType==1){
                    es.push(cn);
                }else{
                    if(cn.nodeType==3 && cn.nodeValue.trim().length>0){
                      es.push(cn);
                    }
                }
            }
        }
        catch(e){}
        return es;
    }\n""");
    code.append(root+"""._cb.g=function(r,a){
        try{
            var i,e=r,l=a.length;
            for(i=0;i<l;i++){
    """);
    code.append("e="+root+"._cb.ge(e)[a[i]];");
    code.append("""
            }
            return e;
        }catch(e){}
        return null;
    };\n""");
    code.append(root+"""._cb.rn=function(a,b){
            try{
                var c=a.parentNode;
                c.insertBefore(b,a);
                c.removeChild(a);
            }catch(e){}
    };\n""");
    code.append(root+"""._cb.sa=function(a,b,c){
        try{
            if(b=="style"){a.style.cssText+=";"+c;return;}
            a.setAttribute(b,c);
        }catch(e){}
    };\n""");
    return "".join(code)