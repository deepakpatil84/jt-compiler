'''
Created on Aug 8, 2012

@author: Deepak Patil
'''
from ConfigReader import WAFConfig
from Analyzer import SGlobal

class JSDebugger:
    
    def __init__(self):
        pass
        
    @staticmethod
    def getFileXML():
        xcode = []
        mcode = []        
        JSDebugger.xxNavigateForXML(SGlobal.root, xcode, mcode)        
        return "<root><pack>" + "".join(xcode) + "</pack><map>{" + ",".join(mcode) + "}</map></root>"
    
            
    @staticmethod
    def xxNavigateForXML(pack, xcode, mcode):                    
        for p in pack.subpackages:
            xcode.append('<' + p + '>')            
            JSDebugger.xxNavigateForXML(pack.subpackages[p], xcode, mcode)            
            xcode.append('</' + p + '>')
        for name in pack.types:
            t = pack.types[name]
            if t.isClass():
                s = str(t.getCLSID())
                xcode.append('<' + name + ' id=\'' + s + '\'/>')                
                mcode.append("'" + s + "':'" + t.fullname + "'")
    
    @staticmethod
    def writeDebugFile(debug_file_path, paths):        
        package_xml = JSDebugger.getFileXML()
        lib_paths = []
        for p in paths:
            if not p.endswith('/'):
                p +='/'                       
            lib_paths.append( '"' + p.replace('\\', '/') + '"')        
        lib_paths = ",\n".join(lib_paths)
        content = ["""
        /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.services;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *  This file auto generated please do not modify
 */
public class WAFDebug extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String oid = request.getParameter("oid");

        if (oid != null) {
            int id = Integer.parseInt(oid);

            if (id == 1) {                
                out.write(\""""
        , package_xml 
        , """\" );                
            } else if (id == 2) {
                String fullname = request.getParameter("id");
                fullname = fullname.replace('.', '/') + ".java";
                String pathlist[] = {"""
        , lib_paths    
        , """};            
                for (String path : pathlist) {
                    path = path + fullname;
                    File f = new File(path);                    
                    if (f.exists()) {
                        FileReader fr = new FileReader(f);
                        char buffer[] = new char[1024 * 4];
                        int readcount = fr.read(buffer);
                        while (readcount > 0) {
                            out.write(buffer, 0, readcount);
                            readcount = fr.read(buffer);
                        }
                    }

                }

            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

        """]
        f = open(debug_file_path, "w");
        f.write("".join(content))
        f.close();
        
            
    
                
                
                    
    
        
