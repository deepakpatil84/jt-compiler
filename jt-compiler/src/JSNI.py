"""
/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
 """
import Tokenizer
from Tokenizer import TokenStream

class NTypeField:
    def __init__(self):
        self.name=None

class NTypeName:
    def __init__(self):
        self.names=None

class NArgType:
    def __init__(self):
        self.names=None

class NDot:
    def __init__(self):
        pass

class NOpenBracket:
    def __init__(self):
        pass

def processNativeCode(code):
    tokens=Tokenizer.tokenizeData("\n".join(code))
    ts= TokenStream(tokens,None)
    rtoks=[]
    t=ts.getCurrentToken()
    
    while not ts.isEnd():
        if t.data.startswith('\'\\u') or t.data.startswith('\'\\U'):            
            t.data=str(int(t.data[3:-1],16))
            rtoks.append(t)            
        elif t.data==".":
            nt=ts.getNextToken()
            if nt.data=="@":
                rtoks.append([NDot()]+readJSNI(ts))
            else:
                rtoks.append(t)
                rtoks.append(nt)
        elif t.data=="@":
            rtoks.append(readJSNI(ts))
        else:
            rtoks.append(t)
        t=ts.getNextToken()
    return rtoks
            
def readJSNI(ts):
    toks=[]    
    names=[]    
    nt=ts.getNextToken()
    names.append(nt)
    t=ts.getNextToken()
    while t.data==".":
        nt=ts.getNextToken()
        names.append(nt)
        t=ts.getNextToken()
    tname=NTypeName()
    tname.names=names
    toks.append(tname)
    if t.data!=":":
        raise Exception('SyntaxError: Expected colon (:) ad class name at Line No:' + str(t.lineno)  +" Position:"+str(t.pos) )
    t=ts.getNextToken()
    if t.data!=":":
        raise Exception('SyntaxError: Expected colon (:) ad class name at Line No:' + str(t.lineno)  +" Position:"+str(t.pos) )
    t=ts.getNextToken()
    f=NTypeField()
    f.name=t
    toks.append(f)
    pi=ts.getCurrentIndex()
    t=ts.getNextToken()
    if t.data!="(":
        ts.setCurrentIndex(pi)
    else:
        argtypes=readArgTypes(ts)
        toks.append(argtypes)
        curToken=ts.getCurrentToken()
        if curToken.data==")":
            curToken=ts.getNextToken()
        if curToken.data=="(":
            toks.append(NOpenBracket())
    return toks

def readArgTypes(ts):
    rvalue =[]
    nt=ts.getNextToken()
    
    if nt.data!=")":        
        types=[]
        at=readArgType(ts)
        types.append(at)
        
        while True:
            curToken=ts.getCurrentToken()
            
            if curToken.data==";":
                curToken=ts.getNextToken()
            
            if curToken.data==")":
                rvalue = types
                break
            at=readArgType(ts)
            types.append(at)
    return rvalue
    

def readArgType(ts):
    at=NArgType()
    t=ts.getCurrentToken()
    
    names=[]
    names.append(t)
    t=ts.getNextToken()
    while t.data=="/":
        t=ts.getNextToken()
        names.append(t)
        t=ts.getNextToken()
    at.names=names
    return at
        
        