"""
/*
 * Copyright 2014 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
 """
import os
import sys
from ConfigReader import WAFConfig, LibConfig
import Compiler
import ViewControllerCompiler
import shutil
import UsageAnalyzer
import CssProcessor
from Analyzer import SGlobal, SHelper, SClass, SCompilationUnit
import hashlib
import SharedTypes
from JSDebugger import JSDebugger



def getWebPath(path):
    
    web_path = os.path.join(path, "web")#netbeans
    
    if not os.path.exists(web_path):        
        web_path = os.path.join(path, "WebContent")#default in eclipse
        
        if not os.path.exists(web_path):
            web_path = None
    
    return web_path

def getSrcPath(path):
    src_path = os.path.join(path, "src", "java")
    
    if not os.path.exists(src_path):        
        src_path = os.path.join(path, "src")
        
        if not os.path.exists(src_path):
            src_path = None
    
    return src_path

def getMinifiedJS(lines):
    """
    ml=[]
    last_line=None
    for l in lines:
        if last_line==None:
            ml.append(l)
        else:
            if las_line.endswith("}")
                ml.append(l)
        last_line=l
    """    
    
    ml = lines
    """
    if WAFConfig.isMinify():
        ml = []
        line = None
        l = ""        
        for l in lines:
            if line == None:
                line = l
            else:        
                if line.endswith(";"):
                    line += l.strip()
                else:
                    if len(line)>0:
                        ml.append(line)                
                    line = l.strip()                 
        if len(line)>0:
            ml.append(line)
    """                
    return "\n".join(ml)
            
def printHelp():
    print "Syntax "
    print "WAFACompiler.py [root-directory-of-source-project]"
    print ""
    
def compileApp(path):
    SGlobal.modified_files = None
    sys.setrecursionlimit(40000)
    
    if not os.path.exists(path):
        print "ERROR: Path not found for source project :" + path
        printHelp()
        sys.exit()
    
    source_root = path
    source_web_path = getWebPath(source_root)
    source_src_path = getSrcPath(source_root)
    
    if source_web_path == None:
        print "ERROR: Not a valid root directory of source project,could not find web directory"
        printHelp()
        sys.exit()
    
    if source_src_path == None:
        print "ERROR: Not a valid root directory of source project,could not find java source directory"
        printHelp()
        sys.exit()
    
    if not(os.path.exists(source_web_path) and os.path.exists(source_src_path)):
        print "ERROR: Not a valid source project directory " + source_root
        printHelp()
        sys.exit()
    
    config_path = os.path.join(source_web_path, "JSTWAFConfig.xml")
    
    if not os.path.exists(config_path):
        print config_path
        print "ERROR: Config file(JSTWAFConfig.xml) not found in source directory . Normally located in [ROOT]/web or [ROOT]WebContents"
        printHelp()
        sys.exit()
    
    WAFConfig.read(config_path)
    target_root = WAFConfig.target_path    
    
    if not os.path.exists(target_root):
        print "ERROR: Path not found for target project :" + target_root
        sys.exit()
    
    if source_root == target_root:
        print "Source and target directory are same. "
        sys.exit(0)
    
    target_web_path = getWebPath(target_root)
    target_src_path = getSrcPath(target_root)
    
    if not(os.path.exists(target_web_path) and os.path.exists(target_src_path)):
        print "ERROR: Not a valid target project dir" + target_root
        sys.exit()
    
    debug_source_paths= None
    client_specific_classes = os.path.join(source_src_path, "client")
    shared_classes = os.path.join(source_src_path, "shared")    
    controllers_path = os.path.join(source_src_path, "controllers")
    modules_path = os.path.join(source_src_path, "modules")
    if WAFConfig.isJSDebuggingEnabled():
        debug_source_paths = []
        debug_source_paths.extend(WAFConfig.lib_paths)
        debug_source_paths.append(source_src_path)
        
    #if os.path.exists(modules_path): 
    #    WAFConfig.lib_paths.append(modules_path)
        
    if os.path.exists(client_specific_classes): 
        WAFConfig.lib_paths.append(client_specific_classes)
    
    if os.path.exists(shared_classes): 
        WAFConfig.lib_paths.append(shared_classes)
    
    model_classes = os.path.join(source_src_path, "server", "services")    
        
    if os.path.exists(model_classes):
        files = os.listdir(model_classes)
        
        for f in files:
            if f != ".svn": 
                model_path = os.path.join(model_classes, f)
                Compiler.processJavaFile(model_path, "Model")
    
    """    
    if os.path.exists(modules_path):
        files = os.listdir(modules_path)
        
        for f in files:
            if f != ".svn" and f.endswith(".java"):
                print "Module"
                #model_path = os.path.join(modules_path, f)
                #Compiler.processJavaFile(model_path, True)
    """
    
    ###################
   
    processed_files = {}
    cv_pairs = {}
    view_codes = {}
    processViewControlerPairs(source_web_path, controllers_path, processed_files, cv_pairs)
    module_pairs = {}
    if os.path.exists(modules_path):        
        processModules(modules_path, processed_files, module_pairs)   
    ###################
    Compiler.compileLib()
    import Java2js_SCS
    import Java2js
    proxy_code = Java2js_SCS.SCS.getProxyCode()
    if proxy_code != None:
        proxy_code = Java2js.getFormatedCode(proxy_code, 0)    
    
        
    
    SGlobal.used_methods = []    
    #SGlobal.processed_refs = [] 
    
    if WAFConfig.isRemoveUnusedCode():
        for vfile in cv_pairs:
            cls = cv_pairs[vfile].decls[0]
            #view_codes[vfile] = ViewControllerCompiler.markRefsForVC(vfile, cls)
            ViewControllerCompiler.markRefsForVC(vfile, cls)
            UsageAnalyzer.analyze(cls)
            
    lib_lines1 = Compiler.getJSForLib()
            
    
    for vfile in cv_pairs:
        cls = cv_pairs[vfile].decls[0]
        cv_pairs[vfile].controller = True
        view_codes[vfile] = ViewControllerCompiler.processControllerViewPair(vfile, cls)
    
   
        
    copyWebContents(source_web_path, target_web_path, view_codes, processed_files)
    copySrcContents(source_src_path, target_src_path)
    copySharedSrcContents(source_src_path, target_src_path)
    copyNecessarySrcFiles(source_src_path, target_src_path)
    #copyContents(source_root,target_root,"",view_codes,processed_files)
    
    if os.path.exists(modules_path):
        module_code=["$ow_module_mapping={}"]        
        #processModules(modules_path, processed_files, module_pairs)    
        
        for mfile in module_pairs:            
            module_file_name=mfile.rpartition(os.path.sep)[2][:-5]+".js"      
            module_pairs[mfile][0].module = True
            t=module_pairs[mfile][0].decls[0]
            module_code.append("$ow_module_mapping[\""+t.fullname+"\"]=\"modules/"+module_file_name+"\";\n")
        #module_code =Java2js.getFormatedCode(module_code,0)
        #print module_code
        lib_lines1=lib_lines1+module_code      
    lib_lines2 = Compiler.getJSForLib2()
    js_path = os.path.join(target_web_path, WAFConfig.getCompiledJSFileName())
    writeFileIfNecessary(js_path, getMinifiedJS(lib_lines1 + lib_lines2))
    
    used_methods_back = SGlobal.used_methods
    print "====="
    print used_methods_back
    print "-----"
    if os.path.exists(modules_path):        
        #processModules(modules_path, processed_files, module_pairs)    
        
        for mfile in module_pairs:
            print "Processing Module",mfile
            (module_cls,module_view_file)=module_pairs[mfile]
            """
            if WAFConfig.isRemoveUnusedCode():
                #SGlobal.used_methods = []                
                #ViewControllerCompiler.markRefsForVC(vfile, module_cls.decls[0])
                #for cls in module_cls.decls:        
                    UsageAnalyzer.analyze(cls)
                module_used_methods=SGlobal.used_methods
                for m in SGlobal.used_methods:
                    if m not in used_methods_back:
                        print m,
            """
            #print ViewControllerCompiler.processControllerViewPair(module_view_file, module_cls.decls[0])
            module_file_name=mfile.rpartition(os.path.sep)[2][:-5]+".js"
            t_modules_path=os.path.join(target_web_path,"modules")
            if not os.path.exists(t_modules_path):
                os.mkdir(t_modules_path)
                
            module_file_path=os.path.join(t_modules_path,module_file_name)
            fh=open(module_file_path,"w")
            fh.write(ViewControllerCompiler.processModuleViewPair(module_cls.decls[0],module_view_file) )
            fh.close()
                

    SGlobal.used_methods = used_methods_back          

    
    css_path = os.path.join(target_web_path, WAFConfig.getCompiledCSSFileName())
    if SGlobal.css_data!=None and len(SGlobal.css_data)>0:
        writeFileIfNecessary(css_path,"".join(SGlobal.css_data))
    
    dir_path = os.path.join(target_src_path, "server", "services")
    if os.path.exists(dir_path) and proxy_code != None:
        writeFileIfNecessary(os.path.join(dir_path, "WAFProxy.java"), proxy_code)
    if WAFConfig.isJSDebuggingEnabled():        
        JSDebugger.writeDebugFile(os.path.join(dir_path, "WAFDebug.java"),debug_source_paths)
        
    print " Application Compiled Successfully. "        
    """
    for md5 in SGlobal.hashes:
        if SGlobal.hashes[md5] > 1:
            print md5, ",", SGlobal.hashes[md5]
    
    print "Chars saved", SGlobal.chars_saved
    """

def processModules(modules_path, processed_files, module_pairs):
    files = os.listdir(modules_path)
    
    for fname in files:
        if fname != ".svn":
            file_path = os.path.join(modules_path, fname)
            
            if os.path.isdir(file_path):
                processModules(file_path, processed_files, module_pairs)
                
            elif fname.endswith(".java"):                       
                module_view_path = os.path.join(modules_path, fname[:-5] + ".view.html")                
                processed_files[file_path] = 1
                if module_view_path!=None:
                    processed_files[file_path] = 1                                        
                    module_pairs[file_path] = [ Compiler.processJavaFile(file_path, "Module") , module_view_path ]
                    module_pairs[file_path][0].module = True

def processViewControlerPairs(source_web_path, controllers_path, processed_files, cv_pairs):
    files = os.listdir(source_web_path)
    
    for fname in files:
        if fname != ".svn":
            f_view_path = os.path.join(source_web_path, fname)
            
            if os.path.isdir(f_view_path):
                processViewControlerPairs(f_view_path, os.path.join(controllers_path, fname), processed_files, cv_pairs)
                                        
            if fname.endswith(".jsp") or fname.endswith(".html"):
                f_controller_path = None        
                
                if fname.endswith(".html"):
                    f_controller_path = os.path.join(controllers_path, fname[:-5] + ".java")
                elif fname.endswith(".jsp"):
                    f_controller_path = os.path.join(controllers_path, fname[:-4] + ".java")
                
                if os.path.exists(f_controller_path):
                    processed_files[f_view_path] = 1
                    processed_files[f_controller_path] = 1
                    cv_pairs[f_view_path] = Compiler.processJavaFile(f_controller_path, "Controller")

def copyNecessarySrcFiles(source, target):
    
    path = os.path.join(target, "com", "jstype", "web", "annotation")
    if not os.path.exists(path):
        os.makedirs(path)
    
    tpath = os.path.join(path, "AsyncRequest.java")    
    cls = SHelper.getClassOnFullName("com.jstype.web.annotation.AsyncRequest")
    #SHelper.printPackages(SGlobal.root)
    spath = cls.scu.filepath
    copyFileIfNecessary(spath, tpath)
    #import com.jstype.core.framework.WAFModel;
    path = os.path.join(target, "com", "jstype", "web", "core")
    
    if not os.path.exists(path):
        os.makedirs(path)
    
    tpath = os.path.join(path, "WAFModel.java")    
    cls = SHelper.getClassOnFullName(LibConfig.P_WAF_MODEL)
    spath = cls.scu.filepath
    copyFileIfNecessary(spath, tpath)
    #import com.jstype.core.framework.AsyncCallback;
    path = os.path.join(target, "com", "jstype", "web", "core")
    
    if not os.path.exists(path):
        os.makedirs(path)
    
    tpath = os.path.join(path, "AsyncCallback.java")    
    cls = SHelper.getClassOnFullName(LibConfig.P_WAF_ASYN_CLBK)
    spath = cls.scu.filepath
    copyFileIfNecessary(spath, tpath)
    #com.jstype.server.Utils;
    #com.jstype.server.JSONSerializable;
    slibpath = None
    
    for path in WAFConfig.lib_paths:
        slibpath = os.path.join(path, LibConfig.P_WEB_SERVER.replace(".",os.path.sep))
        print "XX:",slibpath
        #slibpath = os.path.join(path, "com", "jstype", "web",  "server")
        if os.path.exists(slibpath):
            break
        slibpath = None
    
    if slibpath == None:
        raise Exception("Could not find necessary server classes")
    
    files = os.listdir(slibpath)
    tpathdir = os.path.join(target, LibConfig.P_WEB_SERVER.replace(".",os.path.sep))
    
    if not os.path.exists(tpathdir):
        os.makedirs(tpathdir)
    
    for f in files:
        if f != ".svn":
            spath = os.path.join(slibpath, f)
            tpath = os.path.join(tpathdir, f)
            copyFileIfNecessary(spath, tpath)
    #org.json
    slibpath = None
    for path in WAFConfig.lib_paths:
        slibpath = os.path.join(path, "org", "json")
        if os.path.exists(slibpath):
            break
        slibpath = None
    
    if slibpath == None:
        raise Exception("Could not find json classes in library")
    files = os.listdir(slibpath)
    tpathdir = os.path.join(target, "org", "json",)
    
    if not os.path.exists(tpathdir):
        os.makedirs(tpathdir)
    copySrcContentsSubDir(slibpath, tpathdir)
    

def copySharedSrcContents(source, target):
    
    files = os.listdir(source)
    
    for f in files:
        if f.lower() in ["shared"]:
            spath = os.path.join(source, f)
            tpath = os.path.join(target, f)
            if os.path.isdir(spath):
                if not os.path.exists(tpath):
                    os.mkdir(tpath)
                copySharedSrcContentsSubDir(spath, tpath, "shared")
            else:             
                #copyFileIfNecessary(spath, tpath)
                cls = SHelper.getClassOnFullName("shared." + f[:-5])

                if cls == None: 
                    raise Exception("Shared Path not found " + f) 
                
                code = SharedTypes.ST.getCodeSharedTypeJavaFile(cls, spath)
                writeFileIfNecessary(tpath, code)
                
def copySharedSrcContentsSubDir(source, target, path):
    files = os.listdir(source)
    for f in files:
        if f.lower() in [".svn"]:continue        
        spath = os.path.join(source, f)
        tpath = os.path.join(target, f)
        if os.path.isdir(spath):
            if not os.path.exists(tpath):
                os.mkdir(tpath)
            copySharedSrcContentsSubDir(spath, tpath, path + "." + f)
        else:
            cls = SHelper.getClassOnFullName(path + "." + f[:-5])
            if cls == None: raise Exception("Shared Path not found " + path + "." + f) 
            code = SharedTypes.ST.getCodeSharedTypeJavaFile(cls, spath)
            writeFileIfNecessary(tpath, code)
#==
        
def copySrcContents(source, target):
    files = os.listdir(source)
    for f in files:
        if not f.lower() in [".svn", "client", "controllers", "shared","modules"]:
            spath = os.path.join(source, f)
            tpath = os.path.join(target, f)
            if os.path.isdir(spath):
                if not os.path.exists(tpath):
                    os.mkdir(tpath)
                copySrcContentsSubDir(spath, tpath)
            else:
                copyFileIfNecessary(spath, tpath)
def copySrcContentsSubDir(source, target):
    files = os.listdir(source)
    for f in files:
        if not f.lower() in [".svn"]:    
            spath = os.path.join(source, f)
            tpath = os.path.join(target, f)
            if os.path.isdir(spath):
                if not os.path.exists(tpath):
                    os.mkdir(tpath)
                copySrcContentsSubDir(spath, tpath)
            else:
                copyFileIfNecessary(spath, tpath)

            
def copyWebContents(source, target, view_codes, processed_files):
    files = os.listdir(source)
    for f in files:
        if not f.lower() in [".svn", "meta-inf", "web-inf", "jstwafconfig.xml"]:
            spath = os.path.join(source, f)
            tpath = os.path.join(target, f)
            if os.path.isdir(spath):
                if not os.path.exists(tpath):
                    os.mkdir(tpath)
                copyWebContentsSubDir(spath, tpath, view_codes, processed_files)
            else:
                if view_codes.has_key(spath):
                    writeFileIfNecessary(tpath, view_codes[spath])                
                else:
                    if spath[-4:] == ".css1":
                        ls = CssProcessor.processWAFcss(spath)
                        if ls != None:
                            writeFileIfNecessary(tpath, ls)                        
                    elif not processed_files.has_key(spath):
                        copyFileIfNecessary(spath, tpath)

def copyWebContentsSubDir(source, target, view_codes, processed_files):
    files = os.listdir(source)
    for f in files:
        if not f.lower() in [".svn"]:      
            spath = os.path.join(source, f)
            tpath = os.path.join(target, f)
            if os.path.isdir(spath):
                if not os.path.exists(tpath):
                    os.mkdir(tpath)
                copyWebContentsSubDir(spath, tpath, view_codes, processed_files)
            else:
                if view_codes.has_key(spath):
                    writeFileIfNecessary(tpath, view_codes[spath])                
                else:
                    if spath[-4:] == ".css1":
                        ls = CssProcessor.processWAFcss(spath)
                        if ls != None:
                            writeFileIfNecessary(tpath, ls)                        
                    elif not processed_files.has_key(spath):
                        copyFileIfNecessary(spath, tpath)

            
    
def copyFileIfNecessary(s, t):
    modified = False
    if os.path.exists(t) and os.path.getsize(s) != os.path.getsize(t):
        modified = True
    if (not modified) and os.path.exists(t):
        md5 = hashlib.md5()
        md5.update(open(s, "rb").read())
        s_md5 = md5.digest()
        md5 = hashlib.md5()
        md5.update(open(t, "rb").read())
        t_md5 = md5.digest()
        if t_md5 == s_md5:            
            return
    shutil.copyfile(s, t)

def writeFileIfNecessary(t, data):
    modified = False
    if os.path.exists(t) and os.path.getsize(t) != len(data):
        modified = True
    if (not modified) and os.path.exists(t):
        md5 = hashlib.md5()
        md5.update(open(t, "rb").read())
        s_md5 = md5.digest()
        md5 = hashlib.md5()
        md5.update(data)
        t_md5 = md5.digest()
        if t_md5 == s_md5:            
            return
    f = open(t, "wb")
    f.write(data)
    f.close()
                

if __name__ == "__main__":
    a = sys.argv[0]
    [a, s, f] = a.rpartition(os.sep)    
    if len(sys.argv) == 2:
        path = sys.argv[1]
        path = os.path.realpath(path).replace('\\','/')
        compileApp(path)        
    else:
        print ""
        print "Usage: " + f + " <root_dir_of_source_project>"
        print ""
        sys.exit(0)
    