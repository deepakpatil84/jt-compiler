"""
/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
 """

import sys
class THelper:
    ws_char = [chr(0x0A), chr(0x20), chr(0x09), chr(0x0C), chr(0x0D)]
    sp_char = ['(', ')', '[', ']', '\\', '/', '^', '-', '?', '.', '*', '+', '{', '}', '&', ':', ';', ',', '<', '>', '=', '@', '!', '%', '|', '~']
    opr_1 = ['.', '+', '-', '*', '/', '%', '<', '>', '&', '|', '^', '?', ':', '=', '~', '!']
    opr_2 = ['++', '--', '<<', '<=', '=', '==', '!=', '&&', '||', '+=', '-=', '*=', '/=', '%=', '&=', '|=', '^=']
    opr_3 = ['<<=']

    @staticmethod
    def is_operator(s):
        rvalue = False
        slen = len(s)
        if slen <= 3:
            if slen == 1:
                if not (s.isalpha() or  s.isdigit()):
                    rvalue =  s in THelper.opr_1
            else:
                c = s[0]
                if not ( c.isalpha() or c.isdigit() ):
                    if slen == 2:
                        rvalue = s in THelper.opr_2
                    else:
                        rvalue = s in THelper.opr_3                    
        return rvalue
        
    @staticmethod
    def is_sp_char(s):
        return s in THelper.sp_char
    
    @staticmethod
    def is_whitespace(s):
        return s in THelper.ws_char
class Stream:
    def __init__(self, fname ,its_a_data=False):
        if its_a_data==False:
            self.data = open(fname, "rb").read()
        else:
            self.data = fname
        self.max_len = len(self.data)
        self.cur_pos = 0
        self.peeked = False
        self.peek_count = 0
        self.line_offset = 1
        self.line_number = 1
    
    def peek(self):
        rvalue = None
        self.peeked = True
        if not ( self.cur_pos + self.peek_count >= self.max_len ):            
            rvalue = self.data[self.cur_pos + self.peek_count]
            self.peek_count += 1
        return rvalue
    
    def peeks(self, count):
        rvalue = None
        self.peeked = True
        end = self.cur_pos + self.peek_count + count
        start = self.cur_pos + self.peek_count
        if end < self.max_len:            
            self.peek_count += count
            rvalue = self.data[start:end]
        return rvalue
    
    def read(self):
        rvalue = None
        if self.peeked:#avoiding funciton call to reset
            self.peeked = False
            self.peek_count = 0
        if self.cur_pos < self.max_len:            
            rvalue = self.data[self.cur_pos]
            self.cur_pos += 1
            self.line_offset += 1
            if  rvalue == '\n':
                self.line_number += 1
                self.line_offset = 1
        return rvalue
    
    def reads(self, count):
        rvalue = None
        if self.peeked:
            self.peeked = False
            self.peek_count = 0
        end = self.cur_pos + count
        if end < self.max_len:            
            self.line_offset += count
            self.cur_pos += count
            rvalue = self.data[self.cur_pos:self.cur_pos + count]
        return rvalue
    def reset(self):
        if self.peeked:
            self.peeked = False
            self.peek_count = 0

class TokenStream:
    def __init__(self, tokens, filepath):
        self.filepath = filepath
        self.tokens = tokens
        self.maxIndex = len(tokens)
        self.curIndex = 0
    
    def getCurrentTokenAsItIs(self):
        return self.tokens[self.curIndex]
    
    def getCurrentToken(self):
        rvalue = None
        while True:
            if self.curIndex >= self.maxIndex:
                rvalue = None
                break
            else:
                tok = self.tokens[self.curIndex]
                if tok.data.startswith("/*") or tok.data.startswith("//"):
                    self.curIndex += 1
                else:
                    rvalue = tok
                    break                    
        return rvalue
    
    def getCurrentIndex(self):
        return self.curIndex
    
    def setCurrentIndex(self, index):
        self.curIndex = index
    
    def getNextToken(self):
        self.curIndex += 1
        tok = self.getCurrentToken()
        return tok
    
    def isEnd(self):
        return self.curIndex >= self.maxIndex


class Token:
    def __init__(self, s, lineno, pos):
        self.data = s
        self.lineno = lineno
        self.pos = pos
    def __str__(self):
        return self.data

def tokenizeData(data):
    s=Stream(data,True)
    return tokenizeStream(s,True)

def tokenize(filepath,pws=False,its_a_data=False):
    s = Stream(filepath,its_a_data)
    return tokenizeStream(s,pws)

def tokenizeStream(s,pws=False):
    tokens = []
    c = ""
    nc = ""
    data = ""
    while True:
        c = s.read()        
        if c == None or len(c) == 0:
            break
        nc = s.peek()
        #if is_whitespace(c):continue
        if THelper.is_whitespace(c):
            if pws==True:
                tokens.append(Token(c, s.line_number, s.line_offset))            
        elif c == '/' and nc == '*':
            data = "/*"
            s.read()
            while True:
                if s.peek() == '*' and s.peek()=='/':
                    data += "*/"
                    break
                data += s.read()
            s.reads(2)
            tokens.append(Token(data, s.line_number, s.line_offset))
        elif c == '/' and nc == '/':
            data = '//'
            s.read()
            while True:
                if s.peek() == '\n':
                    break
                data += s.read()
            s.read()
            tokens.append(Token(data, s.line_number, s.line_offset))
        elif c == '\'':
            data = c
            if s.peeked:
                s.peeked = False
                s.peek_count = 0
            while True:
                c = s.peek()
                if c == '\\':
                    data += s.read()
                    data += s.read()
                elif c == '\'':
                    data += s.read()
                    break
                else:
                    data += s.read()
            tokens.append(Token(data, s.line_number, s.line_offset))
        elif c == '\"':
            data = c
            if s.peeked:
                s.peeked = False
                s.peek_count = 0
            while True:
                c = s.peek()
                if c == '\\':
                    data += s.read()
                    data += s.read()
                elif c == '\"':
                    data += s.read()
                    break
                else:
                    data += s.read()
            tokens.append(Token(data, s.line_number, s.line_offset))
        else:
            data = c
            if s.peeked:
                s.peeked = False
                s.peek_count = 0
            if THelper.is_operator(data):
                while THelper.is_operator(data + s.peek()):
                    data += s.read()
                tokens.append(Token(data, s.line_number, s.line_offset))
            elif THelper.is_sp_char(data):
                tokens.append(Token(data, s.line_number, s.line_offset))
            else:
                c = s.peek()
                while not (THelper.is_sp_char(c) or THelper.is_operator(c) or THelper.is_whitespace(c)):
                    s.read()
                    data += c
                    c = s.peek()
                tokens.append(Token(data, s.line_number, s.line_offset))

    return tokens

if __name__ == "__main__":
    tokens = tokenize(sys.argv[1],True)
    for t in tokens:
        print t.data
    sys.exit(0)
    h={}
    for t in tokens:
        if h.has_key(t.data):
            h[t.data]+=1
        else:
            h[t.data]=0
    for k in h:
        #if len(k)<2:continue
        if h[k]<10:continue
        if k.startswith("\""):
            print k+",",h[k]
        else:
            print "\""+k+"\",",h[k]

