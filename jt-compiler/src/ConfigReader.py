"""
/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
 """
from xml.dom.minidom import parse, parseString
import xml.dom
import os
import sys

def raiseUnexpectedError(message=""):
	raise Exception("Unexpected Compiler Error:"+str(message))

class LibConfig:
	MODE = "WEB" #'LIB'
	P_BASE = "com.jstype"
	P_CORE = P_BASE + ".core"
	P_FX   = P_BASE + ".fx"
	P_WEB  = P_BASE + ".web"
	P_WEB_CLIENT = P_WEB + ".client"
	P_WEB_CLIENT_CLS = P_WEB_CLIENT + ".Client"
	P_WEB_DOM = P_WEB_CLIENT + ".dom"
	P_WEB_DOM_ELM = P_WEB_DOM + ".Element"
	
	P_WEB_SERVER = P_WEB + ".server"
	
	P_UI = P_WEB_CLIENT + ".ui"
	P_UI_CONTROL = P_UI + ".UIControl"
	
	P_TYPECAST = P_FX + ".TypeCast"
	P_AUTOBOXING = P_FX + ".Autoboxing"# "com.jstype.core.framework.Autoboxing"
	P_FX_UTIL = P_FX + ".Util"
	P_JSDATE=P_CORE + ".JSDate"
	
	P_JSON_HELPER="com.jstype.web.core.JSONHelper"
	P_JSON_SRLZ = "com.jstype.web.server.JSONSerializable"
	P_BLOCK_METHOD = "com.jstype.web.core.BlockMethod"
	
	P_WAF_MODEL="com.jstype.web.core.WAFModel"
	P_WAF_CNTRL="com.jstype.web.core.WAFController"
	P_WAF_ASYN_CLBK="com.jstype.web.core.AsyncCallback"
	P_MODULE="com.jstype.web.core.Module"
	
	
	
class WAFConfig:
	compiled_js_file	 = "open-waf-compiled.js"
	compiled_css_file	 = "open-waf-compiled.css"
	source_path			 = None
	target_path			 = None
	lib_paths			 = []
	compile_mode  		 = "test" #deply
	target_ide	  		 = "netbeans" #"eclipse"
	target_server 		 = "apache" #glassfish
	proxy_servlet_url 	 = "/WAFProxy" 
	obfuscate			 = False
	minify				 = False
	add_gen_code_line_prefix = True
	gen_code_line_prefix = ""
	remove_unused_code = False
	mark_unused_code	 = False
	unused_code_marker = "/** UNUSED **/"
	enable_array_getter_setter = True
	getter_setter_optimization = False
	waf_root_object_name	   = "$w"
	aggregate_strings	 	= False
	compatible_with_ie_6_7 	= True
	js_debugging_enabled  	= False
	style_sheets 			= []
	css_main_files 			= []
	
	@staticmethod
	def isJSDebuggingEnabled():
		return WAFConfig.js_debugging_enabled
	
	@staticmethod
	def getProxyURL():
		return WAFConfig.proxy_servlet_url
	
	@staticmethod
	def getCompiledJSFileName():
		return WAFConfig.compiled_js_file
	
	@staticmethod
	def getCompiledCSSFileName():
		return WAFConfig.compiled_css_file
	
	@staticmethod
	def getSourcePath():
		return WAFConfig.source_path
	
	@staticmethod
	def getTargetPath():
		return WAFConfig.target_path
	
	@staticmethod
	def getLibPaths():
		return WAFConfig.lib_paths
	
	@staticmethod
	def isCompatibleWithIE67():
		return WAFConfig.compatible_with_ie_6_7
	@staticmethod
	def isMinify():
		return WAFConfig.minify
	@staticmethod
	def isDeploy():
		return WAFConfig.compile_mode == "deploy"
	@staticmethod
	def isTest():
		return not WAFConfig.isDeploy()
	
	@staticmethod
	def getWAFRootObjectName():
		return WAFConfig.waf_root_object_name	
	@staticmethod
	def isStringAggregateEnabled():
		return WAFConfig.aggregate_strings

	@staticmethod
	def isRemoveUnusedCode():
		return WAFConfig.remove_unused_code	
	@staticmethod
	def getBool(value):	return value.lower() == "true"

		
	@staticmethod
	def read(fpath):
		if not os.path.exists(fpath):
			raiseUnexpectedError("JsTypeConfig.xml file not found")
			
		f = open(fpath);
		root = parse(f).childNodes[0]
		for node in root.childNodes:
			if node.nodeType != node.ELEMENT_NODE:continue
			if node.nodeName == "compiled-js-file":WAFConfig.compiled_js_file = node.firstChild.nodeValue.encode('ascii', 'ignore')
			elif node.nodeName == "compiled-css-file":WAFConfig.compiled_css_file = node.firstChild.nodeValue.encode('ascii', 'ignore')
			elif node.nodeName == "target-path":WAFConfig.target_path = node.firstChild.nodeValue.encode('ascii', 'ignore')
			elif node.nodeName == "css-main-files":
				for cnode in node.childNodes:
					if cnode.nodeType != cnode.ELEMENT_NODE:continue
					if cnode.nodeName == "file": 
						path = cnode.firstChild.nodeValue.encode('ascii', 'ignore')
						WAFConfig.css_main_files.append(path)						
			elif node.nodeName == "lib-paths":
				for cnode in node.childNodes:
					if cnode.nodeType != cnode.ELEMENT_NODE:continue
					if cnode.nodeName == "path": 
						path = cnode.firstChild.nodeValue.encode('ascii', 'ignore')
						WAFConfig.lib_paths.append(path)					
			elif node.nodeName == "target-ide": WAFConfig.target_ide = node.firstChild.nodeValue.encode('ascii', 'ignore')
			elif node.nodeName == "target-web-app-server": WAFConfig.target_server = node.firstChild.nodeValue.encode('ascii', 'ignore')
			elif node.nodeName == "compile-profile-name": WAFConfig.compile_mode = node.firstChild.nodeValue.encode('ascii', 'ignore')
			elif node.nodeName == "proxy-servlet-url": WAFConfig.proxy_servlet_url = node.firstChild.nodeValue.encode('ascii', 'ignore')
			elif node.nodeName == "style-sheets":
				for cnode in node.childNodes:
					if cnode.nodeType != cnode.ELEMENT_NODE:continue
					if cnode.nodeName == "style-sheet":
						name = cnode.firstChild.nodeValue.encode('ascii', 'ignore')
						if len(name.strip()) == 0:continue
						WAFConfig.style_sheets.append(name)
			elif node.nodeName == "profile":
				if node.getAttribute("name") != WAFConfig.compile_mode:continue
				for cnode in node.childNodes:
					if cnode.nodeType != cnode.ELEMENT_NODE:continue
					if cnode.nodeName == "enable-javascript-debugging" and cnode.firstChild!=None:WAFConfig.js_debugging_enabled = WAFConfig.getBool(cnode.firstChild.nodeValue.encode('ascii', 'ignore'))					
					elif cnode.nodeName == "obfuscate" and cnode.firstChild!=None: WAFConfig.obfuscate = WAFConfig.getBool(cnode.firstChild.nodeValue.encode('ascii', 'ignore'))
					elif cnode.nodeName == "minify" and cnode.firstChild!=None: WAFConfig.minify = WAFConfig.getBool(cnode.firstChild.nodeValue.encode('ascii', 'ignore'))
					elif cnode.nodeName == "add-prefix-to-gen-code" and cnode.firstChild!=None: WAFConfig.add_gen_code_line_prefix = WAFConfig.getBool(cnode.firstChild.nodeValue.encode('ascii', 'ignore'))
					elif cnode.nodeName == "waf-root-object-name" and cnode.firstChild!=None: WAFConfig.waf_root_object_name = cnode.firstChild.nodeValue.encode('ascii', 'ignore')
					elif cnode.nodeName == "gen-line-code-prefix" and cnode.firstChild!=None: WAFConfig.gen_code_line_prefix = cnode.firstChild.nodeValue.encode('ascii', 'ignore')
					elif cnode.nodeName == "remove-unused-code" and cnode.firstChild!=None: WAFConfig.remove_unused_code = WAFConfig.getBool(cnode.firstChild.nodeValue.encode('ascii', 'ignore'))
					elif cnode.nodeName == "mark-unused-code" and cnode.firstChild!=None: WAFConfig.mark_unused_code = WAFConfig.getBool(cnode.firstChild.nodeValue.encode('ascii', 'ignore'))
					elif cnode.nodeName == "unused-code-marker" and cnode.firstChild!=None: WAFConfig.unused_code_marker = cnode.firstChild.nodeValue.encode('ascii', 'ignore')
					elif cnode.nodeName == "enable-array-getter-setter" and cnode.firstChild!=None: WAFConfig.enable_array_getter_setter = WAFConfig.getBool(cnode.firstChild.nodeValue.encode('ascii', 'ignore'))
					elif cnode.nodeName == "getter-setter-optimization" and cnode.firstChild!=None: WAFConfig.getter_setter_optimization = WAFConfig.getBool(cnode.firstChild.nodeValue.encode('ascii', 'ignore'))
					elif cnode.nodeName == "aggregate-strings" and cnode.firstChild!=None:WAFConfig.aggregate_strings = WAFConfig.getBool(cnode.firstChild.nodeValue.encode('ascii', 'ignore'))
					elif cnode.nodeName == "compatible-with-ie6" and cnode.firstChild!=None: 
						WAFConfig.compatible_with_ie_6_7 = WAFConfig.getBool(cnode.firstChild.nodeValue.encode('ascii', 'ignore'))
	
		"""
		core_lib_found = False
		java_lib_found = False
		for path in WAFConfig.lib_paths:
			if "jstypecorelib" in path.lower():
				core_lib_found = True
			if "jstypejavalib" in path.lower():
				java_lib_found = True
		if java_lib_found == True and core_lib_found == True:return
		a = sys.argv[0]
		[a, s, f] = a.rpartition(os.sep)
		[a, s, f] = a.rpartition(os.sep)
		[a, s, f] = a.rpartition(os.sep)
		if java_lib_found == False:
			WAFConfig.lib_paths.append(a + os.sep + "JsTypeCoreLib" + os.sep + "src" + os.sep)
			#WAFConfig.lib_paths.append(a+os.sep+"JsTypeCoreLib"+os.sep+"build"+os.sep+"JsTypeCoreLib.jar")
		if core_lib_found == False:
			WAFConfig.lib_paths.append(a + os.sep + "JsTypeJavaLib" + os.sep + "src" + os.sep)
		
		"""
					
		#print dir(node)

if __name__ == "__main__":
	WAFConfig.read('D:/Personal/JsTypePublic/projects/ecllipse_pyjc/JsType_Compiler/src/JsType_Config.xml')

		
	
	
	
	
	
