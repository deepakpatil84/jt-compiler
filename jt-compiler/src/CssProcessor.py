"""
/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
 """

        
def processWAFcss(fname):
    from Analyzer import SGlobal
    #print "XX CSS :",fname
    cdata = open(fname).read()
    cp = CSSProcessor(cdata)
    #cp.init(cdata)
    old_macros = SGlobal.css_macros
    old_data = SGlobal.css_data
    
    SGlobal.css_macros = {} 
    SGlobal.css_data = []
    [selectors, macros] = cp.readClass()
    gmacros = SGlobal.css_macros
    
    if gmacros:
        for m in gmacros:
            macros[m] = gmacros[m]
                            
    data = SGlobal.css_data
    for sel in selectors:
        data.append('\n')
        data.append(cp.processSelector(sel, macros, {}))
    SGlobal.css_macros = old_macros
    SGlobal.css_data = old_data       
    return "\n".join(data)                 
    
    
###
class CSSSelector:
    def __init__(self,selectors,body):
        self.selectors = selectors
        self.body      = body
    def __str__(self):
        return " ".join( self.selectors )
class CSSMacro:
    def __init__(self):
        self.name = ""
        self.args = []
        self.body = ""
    def __str__(self):
        return self.name

class CSSProcessor:
    whitespace = [' ','\t','\n','\r']
    seperators = [',', ':', ';', '}', '@', '>', '*', '~','{','(',')']
    
    def __init__(self, data):
        self.data = data
        self.size = len(data)
        self.pos = 0
        self.toks = self.read()
    
    def init(self,data):
        self.data = data
        self.size = len(data)
        self.pos = 0
        self.toks = self.read()
    
    def readComment(self):
        if self.data[self.pos] == '/' and self.data[self.pos + 1] == '*':
            self.pos += 2
            while not (self.data[self.pos] == '*' and self.data[self.pos + 1] == '/'):
                self.pos += 1
            self.pos += 2
    def readString(self):        
        if self.data[self.pos] == "'" or self.data[self.pos] == '"':
            start = self.pos
            char = self.data[self.pos]
            self.pos += 1
            while self.data[self.pos] != char:
                if self.data[self.pos] == '\\':
                    self.pos + 1                    
                self.pos += 1
            self.pos += 1
        return self.data[start:self.pos]
             
    def read(self):                        
        toks = []
        cur_token = ""
        self.pos=0 
        
        while self.pos < self.size:
            char = self.data[self.pos]
            
            if char in CSSProcessor.whitespace:
                if len(cur_token)>0: toks.append(cur_token)
                toks.append(char)
                cur_token = ""
                self.pos += 1 
            
            elif char in CSSProcessor.seperators:
                if len(cur_token)>0: toks.append(cur_token)
                toks.append(char)
                cur_token =""
                self.pos += 1
            
            elif char == '/' and self.data[self.pos+1]=='*':
                self.readComment()
            
            elif char == "'" or char == '"':
                if len(cur_token)>0: toks.append(cur_token)
                toks.append( self.readString() )
                cur_token = ""
            
            else:
                cur_token+=char
                self.pos += 1
        
        return toks
    
    @staticmethod
    def processPartsForMacro(parts,macros,arguments,join_array=True):        
        data = []
        size = len(parts)
        count = 0
        while count < size:
            part = parts[count]
            if part=='@':
                count+=1
                part = parts[count]
                if arguments.has_key(part):
                    data.append(arguments[part])
                    count+=1
                elif macros.has_key(part):
                    macro = macros[part]
                    if len(macro.args)>0:
                        count+=1 
                        while parts[count] in CSSProcessor.whitespace: count+=1
                        params = []
                        if parts[count]=='(':
                            count += 1
                            while parts[count] in CSSProcessor.whitespace: count+=1                            
                            while parts[count]!=')':
                                params.append(parts[count])
                                count+=1
                                while parts[count] in CSSProcessor.whitespace: count+=1
                                if parts[count]==',': 
                                    params.append(',')
                                    count+=1
                                    while parts[count] in CSSProcessor.whitespace: count+=1 
                            params = CSSProcessor.processPartsForMacro(params, macros, arguments, False)
                            nparams=[]
                            for p in params:
                                if p!=',':
                                    nparams.append(p)
                                                                                                     
                            count += 1
                            arg_map = {}
                            param_count = len(macro.args)
                            for index in range(param_count):
                                arg_map[macro.args[index]]=nparams[index]                            
                            data.append(CSSProcessor.processPartsForMacro(macro.body, macros, arg_map))                                                                    
                        else:
                            raise Exception('Expecint ( after ' + part)
                            
                    else:
                        data.append("".join(macro.body))
                        count += 1
                else:
                    raise Exception('Macro not found with name '+part);
                         
                    
            else:
                count+=1
                data.append(part)
        if join_array==True:
            data = "".join(data)
        return data
    @staticmethod
    def processSelector(selector,macros,arguments):        
        data = "".join(selector.selectors) + "{"                
        return data + CSSProcessor.processPartsForMacro(selector.body,macros,arguments)  +"}"                          
    
    def readClass(self):
        toks = self.toks        
        count = 0
        size = len(toks)
        classes = []
        macros = {}
        while count < size:
            while count < size and toks[count] in self.whitespace: 
                count+=1                
            if count >= size: break
            token = toks[count]            
            if token == '@':
                if toks[count+1]=='macro':                    
                    count += 2

                    while count < size and toks[count] in self.whitespace: count+=1
                    
                    name = toks[count]
                    count += 1
                    
                    while count < size and toks[count] in self.whitespace: count+=1
                    
                    if toks[count] == '(':
                        count += 1
                        args = []
                    
                        while count < size and toks[count]!=')':
                            while count < size and toks[count] in self.whitespace: 
                                count+=1                        
                            args.append(toks[count])
                            count+=1
                            while count < size and toks[count] in self.whitespace: 
                                count+=1
                            if toks[count]==',':
                                count+=1
                                while count < size and toks[count] in self.whitespace: 
                                    count+=1                            
                        
                        count+=1
                        
                        while toks[count] in self.whitespace: count+=1
                        
                        if toks[count]=='{':
                            count += 1
                            body = []
                        
                            while count < size and toks[count]!='}':
                                body.append(toks[count])
                                count+=1
                            count += 1
                            
                            m = CSSMacro()
                            m.name = name
                            m.body = body
                            m.args = args
                            if macros.has_key(m.name):
                                raise Exception('Redeclaration of macro with name '+m.name)
                            else:
                                macros[m.name]=m
                        else:
                            raise Exception('Expecting { after css macro '+name)
                        
                    else:
                        values =[]
                        
                        while count < size and toks[count]!='\n':
                            values.append(toks[count])
                            count+=1
                                                
                        while len(values) > 0 and values[len(values)-1]==';' or values[len(values)-1] in self.whitespace:
                            values.pop()
                        m = CSSMacro()
                        m.name =name
                        m.body = values
                        if macros.has_key(m.name):
                            raise Exception('Redeclaration of macro with name '+m.name)
                        else:
                            macros[m.name]=m                                    
                else:
                    count+=1                     
                    
                    while count < size and toks[count]!=';':                        
                        count+=1
                    
                    count+=1
            else:
                selectors = []
                body = []
                
                while count < size and toks[count]!='{':
                    selectors.append(toks[count])
                    count+=1
                
                count+=1
                
                while count < size and toks[count]!='}':
                    body.append(toks[count])
                    count+=1
                
                count+=1
                s=CSSSelector(selectors,body)                
                classes.append(s)
        return [classes,macros]
                
                
  
            
            
        
###    

if __name__ == "__main__":
    pass
