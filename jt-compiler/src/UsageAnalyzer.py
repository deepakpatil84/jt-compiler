"""
/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
 """
import Analyzer
from Analyzer import SGlobal
from ConfigReader import LibConfig

def analyze(rcls):
    um = Analyzer.SGlobal.used_methods
    processed = Analyzer.SGlobal.processed_refs
    if len(um) == 0:
        cls = Analyzer.SHelper.getClassOnFullName(LibConfig.P_BLOCK_METHOD)
        analyzeImpl(cls, um, processed)
        cls = Analyzer.SHelper.getClassOnFullName(LibConfig.P_FX_UTIL)
        analyzeImpl(cls, um, processed)
        cls = Analyzer.SHelper.getClassOnFullName(LibConfig.P_AUTOBOXING)
        analyzeImpl(cls, um, processed)
        cls = Analyzer.SHelper.getClassOnFullName(LibConfig.P_TYPECAST)
        analyzeImpl(cls, um, processed)
        #cls = Analyzer.SHelper.getClassOnFullName("com.jstype.core.framework.RunTimeHelper")
        #analyzeImpl(cls, um, processed)
        cls = Analyzer.SHelper.getClassOnFullName(LibConfig.P_WEB_CLIENT_CLS)
        analyzeImpl(cls, um, processed)
        cls = Analyzer.SHelper.getClassOnFullName("java.lang.Object")
        analyzeImpl(cls, um, processed)
        cls = Analyzer.SHelper.getClassOnFullName("java.util.Iterator")
        analyzeImpl(cls, um, processed)
        cls = Analyzer.SHelper.getClassOnFullName("java.lang.Iterable")
        analyzeImpl(cls, um, processed)
        cls = Analyzer.SHelper.getClassOnFullName("java.lang.Throwable")
        analyzeImpl(cls, um, processed)            
        for i in Analyzer.SGlobal.static_refs:
            um.append(i)
            if not i in processed:
                processed.append(i)
                if Analyzer.SGlobal.method_refs.has_key(i):
                    collect_methods(Analyzer.SGlobal.method_refs[i], um, processed,False)
    analyzeImpl(rcls, um, processed)
    um = list(set(um))
    
    while True:
        tum = checkAbstractMethod(um, processed,not rcls.isModuleClass())
        if tum == None:
            break
        um = tum
    
    while True:
        tum = checkOverridedMethod(um, processed,not rcls.isModuleClass())        
        if tum == None:
            break        
        um = tum
    um = list(set(um))
    # mark implemented method as well             
    Analyzer.SGlobal.used_methods = um

def getOverridedMethods(mid, methods):
    
    if SGlobal.omid.has_key(mid):        
        for m in SGlobal.omid[mid]:            
            if not methods.has_key(m.mid):
                methods[m.mid] = m
            getOverridedMethods(m.mid, methods)
"""
def getOverriddenMethods(mid, methods):
    if SGlobal.omid_.has_key(mid):        
        for m in SGlobal.omid_[mid]:
            if not methods.has_key[m.mid]:
                methods[m.mid] = m
            getOverriddenMethods(m.mid, methods)
"""

def checkOverridedMethod(um, processed,skip_modules):    
    nm = []
    for i in um:
        if SGlobal.omid.has_key(i):
            methods = {}
            getOverridedMethods(i, methods)
            for k in methods:
                if not( k in processed ):
                    m = methods[k]            
                    nm.append(m.mid)                    
                    processed.append(m.mid)
                    if Analyzer.SGlobal.method_refs.has_key(m.mid):
                        collect_methods(Analyzer.SGlobal.method_refs[m.mid], nm, processed,skip_modules)    
            
    rvalue = None
    if len(nm) != 0:
        rvalue = nm + um
        um = rvalue
                
    #nm = checkOverriddenMethods(um, processed)
    
    #if len(nm) != 0:
    #    rvalue = nm + um
    
    return rvalue
"""
def checkOverriddenMethods(um, processed):
    nm = []
    for i in um:        
        if SGlobal.omid_.has_key(i):
            methods = {}
            getOverriddenMethods(i, methods)
            for k in methods:
                if not( k in processed ):
                    m = methods[k]            
                    nm.append(m.mid)                    
                    processed.append(m.mid)
                    if Analyzer.SGlobal.method_refs.has_key(m.mid):
                        collect_methods(Analyzer.SGlobal.method_refs[m.mid], nm, processed)
    return nm
"""

        
def checkAbstractMethod(um, processed,skip_modules):
    found = True
    nm = []
    for i in Analyzer.SGlobal.method_refs:
        m = Analyzer.SGlobal.method_refs[i]
        #if skip_modules:
        #    if m.clazz.getCompilationUnit().isModule():
        #        print "Skipping ",m.name
        #        continue
            
        
        if m.imid != None:
            found = False
            
            for im in m.imid:
                if im.mid in um:
                    found = True
                    break        
            
            if found == True:
                if not ( m.mid in processed):            
                    nm.append(m.mid)                    
                    processed.append(m.mid)
                    if Analyzer.SGlobal.method_refs.has_key(m.mid):
                        collect_methods(Analyzer.SGlobal.method_refs[m.mid], nm, processed,skip_modules)
    rvalue = None
    if len(nm) != 0:    
        rvalue = nm + um
    return rvalue
            
            
       
def analyzeImpl(cls, um, processed):
    print "\n\nXX Analyzing:",cls.fullname
    
    processed = []
    
    for m in cls.methods:
        um.append(m.mid)
        collect_methods(m, um, processed,not cls.isModuleClass())
    
    if cls.isClass():
        for m in cls.constructors:
            um.append(m.mid)
            collect_methods(m, um, processed,not cls.isModuleClass())
    
    
def collect_methods(m, um, processed , skip_modules):
    #print "Collecting For:", m.name, m.clazz.fullname, m.clazz.clsid, skip_modules
    #print m.refs
    if skip_modules:
        if m.clazz.getCompilationUnit().isModule():
            return
    
    
    for i in m.refs:
        #TODO:remove
        """       
        if i == 4218:
            print "XCollecting For:", m.name, m.clazz.fullname, m.clazz.clsid, skip_modules
            print m.clazz.getCompilationUnit().isModule()
            print "CM:",m.mid
            print m.refs        
            print i    
            print m.name
            print m.clazz.fullname
            dfgdsgsdf
        """
        
        um.append(i)
        if not (i in processed):
            #print "Collec:",i
            processed.append(i)
            if Analyzer.SGlobal.method_refs.has_key(i):
                collect_methods(Analyzer.SGlobal.method_refs[i], um, processed,skip_modules)
    
    
